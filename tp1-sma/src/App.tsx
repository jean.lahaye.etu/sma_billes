import React from "react";
import "./App.css";
import Grid from "./components/grid";
import * as SMA from "./class/SMA_Wa_Tor";
import { Ball } from "./class/Ball";
import { Properties } from "./config";
import { Shark } from "./class/Shark";
import { Fish } from "./class/Fish";
import Chart, { BreedData } from "./components/chart";

interface State {
  agents: (Shark | Fish)[];
  env: (Shark | Fish | null)[][];
  sharkData: BreedData[];
  fishData: BreedData[];
}

export default class App extends React.Component<any, State> {
  constructor(props: any) {
    super(props);

    let ballGrid: (Shark | Fish | null)[][] = new Array(Properties.gridSizeX)
      .fill(null)
      .map(() => new Array(Properties.gridSizeY).fill(null));
    let agents = SMA.fillAgents(Properties.nbSharks, Properties.nbFishs, ballGrid);
    console.log(agents);
    this.state = {
      agents,
      env: ballGrid,
      sharkData: [{ y: Properties.nbSharks, label: 0 }],
      fishData: [{ y: Properties.nbFishs, label: 0 }]
    };
  }
  componentDidMount() {
    console.log("App Starting");
    let ticks = 0;
    window.resizeBy(Properties.canvasSizeX, Properties.canvasSizeY); // Do not work on many browsers
    this.loop(ticks);
  }

  loop(ticks: number) {
    if (Properties.nbTicks > 0 && Properties.nbTicks === ticks) {
      return;
    }
    setTimeout(() => {
      let gridAndAgents = SMA.runOnce(this.state.agents, this.state.env);
      let ballGrid = gridAndAgents.grid;
      let sharkData = this.state.sharkData;
      let fishData = this.state.fishData;
      sharkData.push({
        y: gridAndAgents.agents.filter(a => a.type === "Shark" && a.isLiving).length,
        label: ticks + 1
      });
      fishData.push({ y: gridAndAgents.agents.filter(a => a.type === "Fish" && a.isLiving).length, label: ticks + 1 });
      this.setState({
        agents: gridAndAgents.agents,
        sharkData: sharkData,
        fishData: fishData
      });

      if (ticks % Properties.refresh === 0) {
        this.setState({ env: ballGrid });
      }
      ticks++;
      this.loop(ticks);
    }, Properties.delay);
  }

  render() {
    return (
      <div className="App">
        <Grid environment={this.state.env}></Grid>
        <Chart fishData={this.state.fishData} sharkData={this.state.sharkData}></Chart>
      </div>
    );
  }
}
