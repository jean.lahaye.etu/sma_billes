export const Properties: Config = {
  torus: true,
  gridSizeX: 40,
  gridSizeY: 80,
  canvasSizeX: 1000, // Not wokring because of browsers
  canvasSizeY: 1000, // Idem
  boxSize: 10,
  delay: 10,
  scheduling: "sequential", // Not used
  nbTicks: 0,
  grid: true,
  trace: false, // Not used
  seed: 0,
  refresh: 1,
  nbParticles: 12,
  fishBreedTime: 2,
  sharkBreedTime: 5,
  sharkStarveTime: 3,
  nbSharks: 30,
  nbFishs: 300
};

// #ffe54a	(255,229,74)    Y
// #98ff6e	(152,255,110)   GL
// #a1f8ff	(161,248,255)   LB
// #00ae7d	(0,174,125)     DG
// #80d819	(128,216,25)    G

interface Config {
  torus: boolean;
  gridSizeX: number;
  gridSizeY: number;
  canvasSizeX: number;
  canvasSizeY: number;
  boxSize: number;
  delay: number;
  scheduling: Scheduling;
  nbTicks: number;
  grid: boolean;
  trace: boolean;
  seed: number;
  refresh: number;
  nbParticles: number;
  fishBreedTime: number;
  sharkBreedTime: number;
  sharkStarveTime: number;
  nbSharks: number;
  nbFishs: number;
}

type Scheduling = "equitable" | "sequential" | "random";
