import { Agent } from "./Agent";
import { Avatar } from "./Avatar";

export type Hunter = { type: "Hunter" } & Agent;

export const decide = (hunter: Hunter, grid: Agent[][]) => {
  let bestCell: Agent = hunter;
  for (let i = hunter.posX - 1; i <= hunter.posX + 1; i++) {
    if (i < 0) i = 0;
    else if (i >= grid.length) break;
    for (let j = hunter.posY - 1; j <= hunter.posY + 1; j++) {
      if (j < 0) j = 0;
      else if (j >= grid[0].length) break;
      if (grid[i][j].distance <= bestCell.distance && grid[i][j].distance !== -1 && grid[i][j].type !== "Hunter") {
        bestCell = grid[i][j];
        if (grid[i][j].type === "Avatar") {
          (grid[i][j] as Avatar).isLiving = false;
        }
      }
    }
  }
  hunter.posX = bestCell.posX;
  hunter.posY = bestCell.posY;
};
