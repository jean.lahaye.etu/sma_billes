export interface Agent {
  posX: number;
  posY: number;
  distance: number;
  type: string;
}
