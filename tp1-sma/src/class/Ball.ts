import { Properties } from "./../config";
export interface Ball {
  posX: number;
  posY: number;
  pasX: number;
  pasY: number;
  color: string;
}

export const decide = (ball: Ball, ballGrid: (Ball | null)[][]) => {
  Properties.torus ? decideWhithTorus(ball, ballGrid) : decideWhithoutTorus(ball, ballGrid);
};

function decideWhithTorus(ball: Ball, ballGrid: (Ball | null)[][]) {
  var nextX, nextY = null;
  if (ball.posX + ball.pasX >= ballGrid.length) {
    nextX = 0;
  } else if (ball.posX + ball.pasX < 0) {
    nextX = ballGrid.length-1;
  } else {
    nextX = ball.posX + ball.pasX;
  }
  if (ball.posY + ball.pasY >= ballGrid[0].length) {
    nextY = 0;
  } else if (ball.posY + ball.pasY < 0) {
    nextY = ballGrid[0].length-1;
  } else {
    nextY = ball.posY + ball.pasY;
  }
  var nextPos = ballGrid[nextX][nextY];
  if (nextPos == null) {
    ballGrid[ball.posX][ball.posY] = null;
    ball.posX = nextX;
    ball.posY = nextY;
    ballGrid[ball.posX][ball.posY] = ball;
  } else {
    // Il y a une bille, on échange de direction (mais on ne bouge pas)
    var lastPasX = ball.pasX;
    var lastPasY = ball.pasY;
    ball.pasX = nextPos.pasX;
    ball.pasY = nextPos.pasY;
    nextPos.pasX = lastPasX;
    nextPos.pasY = lastPasY;
    ball.color = "red";
    nextPos.color = "red";
 }
}

function decideWhithoutTorus(ball: Ball, ballGrid: (Ball | null)[][]) {
  if (ball.posX + ball.pasX >= ballGrid.length || ball.posX + ball.pasX < 0) {
    ball.pasX = -ball.pasX;
  }
  if (ball.posY + ball.pasY >= ballGrid[0].length || ball.posY + ball.pasY < 0) {
    ball.pasY = -ball.pasY;
  }
  var nextPos = ballGrid[ball.posX + ball.pasX][ball.posY + ball.pasY];
  if (nextPos == null) {
    ballGrid[ball.posX][ball.posY] = null;
    ball.posX += ball.pasX;
    ball.posY += ball.pasY;
    ballGrid[ball.posX][ball.posY] = ball;
  } else {
     // Il y a une bille, on échange de direction (mais on ne bouge pas)
     var lastPasX = ball.pasX;
     var lastPasY = ball.pasY;
     ball.pasX = nextPos.pasX;
     ball.pasY = nextPos.pasY;
     nextPos.pasX = lastPasX;
     nextPos.pasY = lastPasY;
     ball.color = "red";
    nextPos.color = "red";
  }
}