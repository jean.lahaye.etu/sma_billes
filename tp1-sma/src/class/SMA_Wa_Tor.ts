import { Fish } from "./Fish";
import * as FishFunc from "./Fish";
import { Shark } from "./Shark";
import * as SharkFunc from "./Shark";
import { Properties } from "./../config";
import * as Random from "./Random";

export const decide = (
  agent: Shark | Fish,
  grid: (Shark | Fish | null)[][],
  agents: (Shark | Fish)[]
) => {
  switch (agent.type) {
    case "Fish":
      FishFunc.decide(agent as Fish, grid, agents);
      break;
    case "Shark":
      SharkFunc.decide(agent as Shark, grid, agents);
      break;
    default:
      return;
  }
};

export const generateShark = (grid: (Shark | Fish | null)[][]): Shark => {
  let newX = Random.nextnumber(grid.length);
  let newY = Random.nextnumber(grid[0].length);
  while (grid[newX][newY] !== null) {
    newX = Random.nextnumber(grid.length);
    newY = Random.nextnumber(grid[0].length);
  }
  let shark = {
    type: "Shark",
    posX: newX,
    posY: newY,
    starvationTime: Properties.sharkStarveTime + 0,
    sharkBreedTime: Properties.sharkBreedTime + 0,
    isLiving: true,
    isNew: true
  } as Shark;
  grid[newX][newY] = shark;
  return shark;
};

export const generateFish = (grid: (Shark | Fish | null)[][]): Fish => {
  let newX = Random.nextnumber(grid.length);
  let newY = Random.nextnumber(grid[0].length);
  while (grid[newX][newY] !== null) {
    newX = Random.nextnumber(grid.length);
    newY = Random.nextnumber(grid[0].length);
  }
  let fish = {
    type: "Fish",
    posX: newX,
    posY: newY,
    fishBreedTime: Properties.fishBreedTime + 0,
    isLiving: true,
    isNew: true
  } as Fish;
  grid[newX][newY] = fish;
  return fish;
};

export const fillAgents = (
  nbSharks: number,
  nbFish: number,
  grid: (Shark | Fish | null)[][]
) => {
  let agents = Array<Shark | Fish>();
  for (let i = 0; i < nbFish; i++) {
    agents.push(generateFish(grid));
  }
  for (let i = 0; i < nbSharks; i++) {
    let shark = generateShark(grid);
    agents.push(shark);
    console.log(shark);
  }
  return agents;
};

export const runOnce = (
  agents: (Shark | Fish)[],
  grid: (Shark | Fish | null)[][]
) => {
  let deleted = agents.filter(a => !a.isLiving);
  deleted.forEach(a => (grid[a.posX][a.posY] = null));
  agents = agents.filter(a => a.isLiving);
  agents.forEach(a => decide(a, grid, agents));

  return { grid: grid, agents: agents };
};

export const run = (
  nb: number,
  agents: (Shark | Fish)[],
  grid: (Shark | Fish | null)[][]
) => {
  for (let i = 0; i < nb; i++) runOnce(agents, grid);
};
