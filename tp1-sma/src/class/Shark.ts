import { Properties } from "./../config";
import { Fish, Position } from "./Fish";

export type Shark = { type: "Shark" } & SharkBody;
export interface SharkBody {
  posX: number;
  posY: number;
  starvationTime: number;
  sharkBreedTime: number;
  isLiving: boolean;
  isNew: boolean;
}

export const decide = (shark: Shark, grid: (Shark | Fish | null)[][], agents: (Shark | Fish)[]) => {
  if (shark.starvationTime === 0) shark.isLiving = false;
  else {
    shark.starvationTime--;
    if (shark.isNew) shark.isNew = false;
    if (shark.sharkBreedTime > 0) shark.sharkBreedTime--;
    let accessibleCells = Properties.torus
      ? getAccessiblePositionsWithTorus(shark, grid)
      : getAccessiblePositionsWithoutTorus(shark, grid);
    if (accessibleCells.length > 0) {
      let destinationCell = accessibleCells[Math.floor(Math.random() * accessibleCells.length)];
      if (shark.sharkBreedTime === 0) {
        let newShark = {
          type: "Shark",
          posX: shark.posX,
          posY: shark.posY,
          starvationTime: Properties.sharkStarveTime,
          sharkBreedTime: Properties.sharkBreedTime,
          isLiving: true,
          isNew: true
        } as Shark;
        grid[shark.posX][shark.posY] = newShark;
        agents.push(newShark);
        shark.sharkBreedTime = Properties.sharkBreedTime;
      } else {
        grid[shark.posX][shark.posY] = null;
      }
      let nextCell = grid[destinationCell.X][destinationCell.Y];
      if (nextCell !== null) {
        if (nextCell.type === "Fish") {
          nextCell.isLiving = false;
          shark.starvationTime = Properties.sharkStarveTime;
        }
      }
      grid[destinationCell.X][destinationCell.Y] = shark;
      shark.posX = destinationCell.X;
      shark.posY = destinationCell.Y;
    }
  }
};

function getAccessiblePositionsWithoutTorus(shark: Shark, grid: (Shark | Fish | null)[][]): Position[] {
  let fishPositions = Array<Position>();
  let nullPositions = Array<Position>();
  let posX = shark.posX,
    posY = shark.posY;
  for (var i = -1; i < 2; i++) {
    if (posX + i < 0) continue;
    if (posX + i >= grid.length) break;
    for (var j = -1; j < 2; j++) {
      if (posY + j < 0) continue;
      if (posY + j >= grid[0].length) break;
      if (grid[posX + i][posY + j] !== null && grid[posX + i][posY + j]?.type === "Fish") {
        fishPositions.push({ X: posX + i, Y: posY + j });
      } else if (grid[posX + i][posY + j] === null) {
        nullPositions.push({ X: posX + i, Y: posY + j });
      }
    }
  }
  if (fishPositions.length > 0) return fishPositions;
  else return nullPositions;
}

function getAccessiblePositionsWithTorus(shark: Shark, grid: (Shark | Fish | null)[][]): Position[] {
  let fishPositions = Array<Position>();
  let nullPositions = Array<Position>();
  let posX = shark.posX,
    posY = shark.posY;
  for (var i = -1; i < 2; i++) {
    let XtoLook = posX + i;
    if (XtoLook < 0) XtoLook = grid.length - 1;
    if (XtoLook >= grid.length) XtoLook = 0;
    for (var j = -1; j < 2; j++) {
      let YtoLook = posY + j;
      if (YtoLook < 0) YtoLook = grid.length - 1;
      if (YtoLook >= grid[0].length) YtoLook = 0;
      if (grid[XtoLook][YtoLook] !== null && grid[XtoLook][YtoLook]?.type === "Fish") {
        fishPositions.push({ X: XtoLook, Y: YtoLook });
      } else if (grid[XtoLook][YtoLook] === null) {
        nullPositions.push({ X: XtoLook, Y: YtoLook });
      }
    }
  }
  if (fishPositions.length > 0) return fishPositions;
  else return nullPositions;
}
