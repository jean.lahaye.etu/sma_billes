import { Properties } from "./../config";

//const seedrandom = require("seedrandom");
const rng = /*Properties.seed === 0 ? */Math.random/* : seedrandom(Properties.seed)*/;

export const nextnumber = (max: number) => Math.floor(rng() * Math.floor(max));
