import { Ball, decide } from "./Ball";
import * as Random from "./Random";

export const generateUniqueBall = (ballGrid: (Ball | null)[][]): Ball => {
  let newX = Random.nextnumber(ballGrid.length);
  let newY = Random.nextnumber(ballGrid[0].length);
  console.log(newX + " - " + newY);
  while (ballGrid[newX][newY] !== null) {
    newX = Random.nextnumber(ballGrid.length);
    newY = Random.nextnumber(ballGrid[0].length);
  }
  let ball = {
    posX: newX,
    posY: newY,
    pasX: Random.nextnumber(3) - 1,
    pasY: Random.nextnumber(3) - 1,
    color: "black"
  };
  ballGrid[newX][newY] = ball;
  return ball;
};

export const fillAgents = (nbBall: number, ballGrid: (Ball | null)[][]) => {
  let agents = Array<Ball>(nbBall);
  for (let i = 0; i < agents.length; i++) {
    agents[i] = generateUniqueBall(ballGrid);
  }
  return agents;
};

export const runOnce = (agents: Ball[], ballGrid: (Ball | null)[][]) => {
  //shuffleBalls(agents);
  agents.forEach(a => decide(a, ballGrid));
  return ballGrid;
};

export const shuffleBalls = (agents: Ball[]) => {
  for (let i = agents.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [agents[i], agents[j]] = [agents[j], agents[i]];
  }
  return agents;
};

export const run = (
  nb: number,
  agents: Ball[],
  ballGrid: (Ball | null)[][]
) => {
  for (let i = 0; i < nb; i++) runOnce(agents, ballGrid);
};
