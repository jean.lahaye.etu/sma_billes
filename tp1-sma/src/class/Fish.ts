import { Properties } from "./../config";
import { Shark } from "./Shark";

export type Fish = { type: "Fish" } & FishBody;
export interface FishBody {
  posX: number;
  posY: number;
  fishBreedTime: number;
  isLiving: boolean;
  isNew: boolean;
}

export interface Position {
  X: number;
  Y: number;
}

export const decide = (fish: Fish, grid: (Shark | Fish | null)[][], agents: (Shark | Fish)[]) => {
  if (fish.isLiving) {
    if (fish.isNew) fish.isNew = false;
    if (fish.fishBreedTime > 0) fish.fishBreedTime--;
    let accessibleCells = Properties.torus
      ? getAccessiblePositionsWithTorus(fish, grid)
      : getAccessiblePositionsWithoutTorus(fish, grid);
    if (accessibleCells.length > 0) {
      let destinationCell = accessibleCells[Math.floor(Math.random() * accessibleCells.length)];
      if (fish.fishBreedTime === 0) {
        grid[fish.posX][fish.posY] = {
          type: "Fish",
          posX: fish.posX,
          posY: fish.posY,
          fishBreedTime: Properties.fishBreedTime,
          isLiving: true,
          isNew: true
        } as Fish;
        agents.push(grid[fish.posX][fish.posY] as Shark | Fish);
        if (Properties.trace) console.log();
        fish.fishBreedTime = Properties.fishBreedTime;
      } else {
        grid[fish.posX][fish.posY] = null;
      }
      grid[destinationCell.X][destinationCell.Y] = fish;
      fish.posX = destinationCell.X;
      fish.posY = destinationCell.Y;
    }
  }
};

function getAccessiblePositionsWithoutTorus(fish: Fish, grid: (Shark | Fish | null)[][]): Position[] {
  let positions = Array<Position>();
  let posX = fish.posX,
    posY = fish.posY;
  for (var i = -1; i < 2; i++) {
    if (posX + i < 0) continue;
    if (posX + i >= grid.length) break;
    for (var j = -1; j < 2; j++) {
      if (posY + j < 0) continue;
      if (posY + j >= grid[0].length) break;
      if (grid[posX + i][posY + j] === null) {
        positions.push({ X: posX + i, Y: posY + j });
      }
    }
  }
  return positions;
}

function getAccessiblePositionsWithTorus(fish: Fish, grid: (Shark | Fish | null)[][]): Position[] {
  let positions = Array<Position>();
  let posX = fish.posX,
    posY = fish.posY;
  for (var i = -1; i < 2; i++) {
    let XtoLook = posX + i;
    if (XtoLook < 0) XtoLook = grid.length - 1;
    if (XtoLook >= grid.length) XtoLook = 0;
    for (var j = -1; j < 2; j++) {
      let YtoLook = posY + j;
      if (YtoLook < 0) YtoLook = grid[0].length - 1;
      if (YtoLook >= grid[0].length) YtoLook = 0;
      if (grid[XtoLook][YtoLook] === null) {
        positions.push({ X: XtoLook, Y: YtoLook });
      }
    }
  }
  return positions;
}
