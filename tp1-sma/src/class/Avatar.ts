import { Properties } from "../config";
import { Agent } from "./Agent";

export type Avatar = {
  dirX: number;
  dirY: number;
  nextAction: string;
  type: "Avatar";
  isLiving: boolean;
} & Agent;

export const decide = (avatar: Avatar, grid: Agent[][]) => {
  Properties.torus ? moveWithTorus(avatar, grid) : moveWithoutTorus(avatar, grid);
  noteDistances(avatar, grid);
};

function noteDistances(avatar: Avatar, grid: Agent[][]) {
  //1er passage, on nettoie (on met tout à -2, car les -1 sont des murs et ne doivent pas être changés)
  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[0].length; j++) {
      if (grid[i][j].distance !== -1) grid[i][j].distance = -2;
    }
  }
  avatar.distance = 0;
  checkVoisins(avatar, grid);
}

function checkVoisins(agent: Agent, grid: Agent[][]) {
  let voisinsAVisiter = Array<Agent>();
  for (let i = agent.posX - 1; i <= agent.posX + 1; i++) {
    if (i < 0) i = 0;
    else if (i >= grid.length) break;
    for (let j = agent.posY - 1; j <= agent.posY + 1; j++) {
      if (j < 0) j = 0;
      else if (j >= grid[0].length) break;
      if (grid[i][j].distance === -2 || agent.distance + 1 < grid[i][j].distance) {
        grid[i][j].distance = agent.distance + 1;
        voisinsAVisiter.push(grid[i][j]);
      }
    }
  }
  voisinsAVisiter.forEach(v => checkVoisins(v, grid));
}

function moveWithTorus(avatar: Avatar, grid: Agent[][]) {
  if (avatar.posX + avatar.dirX < 0) avatar.posX = grid.length - 1;
  else if (avatar.posX + avatar.dirX >= grid.length) avatar.posX = 0;
  else avatar.posX += avatar.dirX;
  if (avatar.posY + avatar.dirY < 0) avatar.posY = grid[0].length - 1;
  if (avatar.posY + avatar.dirY >= grid[0].length) avatar.posY = 0;
  else avatar.posY += avatar.dirY;
}

function moveWithoutTorus(avatar: Avatar, grid: Agent[][]) {
  if (avatar.posX + avatar.dirX > 0 && avatar.posX + avatar.dirX < grid.length - 1) {
    avatar.posX += avatar.dirX;
  }
  if (avatar.posY + avatar.dirY > 0 && avatar.posY + avatar.dirY < grid[0].length - 1) {
    avatar.posY += avatar.dirY;
  }
}
