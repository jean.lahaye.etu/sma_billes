import React from "react";
import { Fish } from "../class/Fish";
import { ReactComponent as FishComponent } from "../svg/fish_b.svg";
import { Properties } from "../config";
import { Shark } from "../class/Shark";

export interface Props {
  agent: Fish | Shark | null;
}

interface State {}

export default class Cell extends React.Component<Props, State> {
  renderAgent = (agent: Fish | Shark | null) => {
    if (agent?.isLiving === false) {
      return "";
    }
    switch (agent?.type) {
      case "Fish":
        let fishColor = "green";
        if (agent.isNew) {
          fishColor = "yellow";
        }
        return <p style={{ backgroundColor: fishColor }}>F</p>;
      case "Shark":
        let sharkColor = "red";
        if (agent.isNew) {
          sharkColor = "pink";
        }
        return <p style={{ backgroundColor: sharkColor }}>S</p>;
      default:
        return "";
    }
  };

  render() {
    return this.renderAgent(this.props.agent);
  }
}
