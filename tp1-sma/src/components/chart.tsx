import React from "react";
import * as test from "../assets/canvasjs.react";
export interface BreedData {
  y: number;
  label: number;
}
export interface Props {
  sharkData: BreedData[];
  fishData: BreedData[];
}

interface State {}

export default class Chart extends React.Component<Props, State> {
  private chart: any;
  private sharkData: BreedData[];
  private fishData: BreedData[];

  constructor(props: Props) {
    super(props);
    this.sharkData = props.sharkData;
    this.fishData = props.fishData;
  }
  componentDidUpdate = () => {
    this.sharkData = this.props.sharkData;
    this.fishData = this.props.fishData;
    /*this.chart.options.data[0].dataPoints = this.props.sharkData;
    this.chart.options.data[1].dataPoints = this.props.fishData;*/
    this.chart.render();
  };
  render() {
    const options = {
      animationEnabled: true,
      title: {
        text: "Number of Breeds over time"
      },
      axisY: {
        title: "Number of Breeds",
        includeZero: false
      },
      toolTip: {
        shared: true
      },
      data: [
        {
          type: "spline",
          name: "Shark",
          showInLegend: true,
          dataPoints: this.sharkData
        },
        {
          type: "spline",
          name: "Fish",
          showInLegend: true,
          dataPoints: this.fishData
        }
      ]
    };
    return (
      <div>
        <test.default.CanvasJSChart options={options} onRef={(ref: any) => (this.chart = ref)} />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}
