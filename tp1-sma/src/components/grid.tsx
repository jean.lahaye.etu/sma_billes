import React from "react";
import Cell from "./cell";
import { Properties } from "../config";
import { Shark } from "../class/Shark";
import { Fish } from "../class/Fish";

export interface Props {
  environment: (Shark | Fish | null)[][];
}

interface State {}

export default class Grid extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  createTable = () => {
    let table = [];
    for (let x = 0; x < Properties.gridSizeX; x++) {
      let children = [];
      for (let y = 0; y < Properties.gridSizeY; y++) {
        children.push(
          <td
            key={"td-" + x + y}
            style={{
              width: Properties.boxSize,
              minWidth: Properties.boxSize,
              height: Properties.boxSize,
              minHeight: Properties.boxSize,
              fontSize: Properties.boxSize * 0.85
            }}
          >
            <Cell key={"cell" + x + y} agent={this.props.environment[x][y]} />
          </td>
        );
      }
      table.push(<tr key={"tr-" + x}>{children}</tr>);
    }
    return table;
  };

  render() {
    return (
      <div className={Properties.grid ? "table-visible" : ""}>
        <table>
          <tbody>{this.createTable()}</tbody>
        </table>
      </div>
    );
  }
}
